# Instructions

## In order to push the docker image to container registry, we are storing credentials in variables

* To generate credentials
    * In the top-right corner, select your avatar.
    * Select Edit profile.
    * In the left sidebar, select Access Tokens.
    * Enter a name and optional expiry date for the token.
    * Select the scope as API.
    * Select Create personal access token.

* Let's store the token in project variables now
    * Go to repoistory page
    * Settings -> CI/CD
    * Add variables
    * Put name as "CONTAINER_TOKEN" and paste the value you got from personal access token

## That's it, we are done, this will build your Docker Image, and push to Container Registry
